# atomizer-test-console

[![CI](https://github.com/devnw/atomizer-test-console/workflows/CI/badge.svg)](https://github.com/devnw/atomizer-test-console/actions)
[![Go Report Card](https://goreportcard.com/badge/github.com/devnw/atomizer-test-console)](https://goreportcard.com/report/github.com/devnw/atomizer-test-console)
[![codecov](https://codecov.io/gh/devnw/atomizer-test-console/branch/master/graph/badge.svg)](https://codecov.io/gh/devnw/atomizer-test-console)
[![GoDoc](https://godoc.org/github.com/devnw/atomizer-test-console?status.svg)](https://pkg.go.dev/github.com/devnw/atomizer-test-console)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)

