module github.com/devnw/atomizer-test-console

go 1.13

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/devnw/alog v1.0.1
	github.com/devnw/atomizer v0.0.0-20200425014640-bf9d01a48c35
	github.com/google/uuid v1.1.1
	github.com/streadway/amqp v0.0.0-20190827072141-edfb9018d271
)
